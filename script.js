
//3. Create a trainer object using object literals.
let trainer = {};


//4. Initialize/add the following trainer object properties:
		// Name (String)
		// Age (Number)
		// Pokemon (Array)
		// Friends (Object with Array values for properties)

trainer.name = `Ash Ketchum`
trainer.age = 10
trainer.pokemon = [`Pikachu`, `Charizard`]
trainer.friends = {
	kanto: [`Brock`, `Misty`],
	hoenn: [`May`, `Max`]
}


// 5. Initialize/add the trainer object method named talk that prints out the message Pikachu! I choose you!
trainer.talk = () => {
	console.log(`Pikachu I choose you!`)
}


console.log(trainer)


// 6. Access the trainer object properties using dot and square bracket notation.
console.log(trainer.name)

console.log(trainer["age"])

// 7. Invoke/call the trainer talk object method.
console.log(trainer.talk())




// Create a constructor for creating a pokemon with the following properties:
	// Name (Provided as an argument to the contructor)
	// Level (Provided as an argument to the contructor)
	// Health (Create an equation that uses the level property)
	// Attack (Create an equation that uses the level property)

function Pokemon(name, lvl){
	this.name = name;
	this.level = lvl;
	this.health = lvl * 3
	this.attack = lvl
	this.intro = function(opponent){
		//const {name} = opponent	//object destructuring

		console.log(`Hi I'm ${this.name} and this is ${opponent.name}`)
	};

	// 10. Create a tackle method that will subtract the health property of the target pokemon object with the attack property of the object that used the tackle method.
	this.tackle = (target) => {
		// console.log(target)	//object


		//subtract the health property of the target pokemon object with the attack property of the object that used the tackle method.
		console.log(target.health -= this.attack)
		const targetHealth = target.health -= this.attack

		// 12. Create a condition in the tackle method that if the health property of the target pokemon object is less than or equal to 0 will invoke the faint method.
		if(targetHealth <= 0){
			target.faint()
		}

	};
	this.faint = () => {

		// 11. Create a faint method that will print out a message of targetPokemon has fainted.
		console.log(`${this.name} has fainted`);
	}
}


// 9. Create/instantiate several pokemon object from the constructor with varying name and level properties.

let Pikachu = new Pokemon("Pikachu", 12)
console.log(Pikachu)

let Geodude = new Pokemon("Geodude", 8)
console.log(Geodude)


let Mewtwo = new Pokemon("Mewtwo", 100)
console.log(Mewtwo)





// 13. Invoke the tackle method of one pokemon object to see if it works as intended.
Geodude.tackle(Pikachu)
Pikachu.tackle(Geodude)


// Geodude.tackle(Pikachu)
// Geodude.tackle(Pikachu)